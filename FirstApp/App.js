
import React, {Component} from 'react';
import {StyleSheet, Text, ScrollView, Image} from 'react-native';




export default class App extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.welcome}>Amor a criacao divina</Text>
        <Text style={styles.cebola}>Os caminhos da governanca ambiental</Text>
        <Text style={styles.cebola}>Katia Silene O. Maia</Text>
        <Image style={styles.img} source={{uri: 'https://www.eviseu.com/livro/f2201f5191c4e92cc5af043eebfd0946@1X.jpg'}}
        style={{width: 350, height: 800}} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#36BEFF',
  },
  img:{
    alignSelf: 'center',
  },
  welcome: {
    fontSize: 40,
    fontWeight: "600",
    textAlign: 'center',
    margin: 10,
    color: '#31E88C'
  },
  cebola: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    marginBottom: 5,
    marginTop: 10,
  },
});
