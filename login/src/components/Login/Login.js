import React, {Component} from 'react';
import {View, Image, StyleSheet, Button} from 'react-native';
import t from 'tcomb-form-native';

const Form = t.form.Form;
const User = t.struct({
	email: t.String,
	username: t.String,
	password: t.String,
	terms: t.Boolean
  });

export default class Login extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/cebola-flat.png')}/>
				</View>
				<View style={styles.form}>
        			<Form type={User} /> 
					<Button
          				
						  title="Você aceita a Cebola?"
						  color="#841584"
						  accessibilityLabel="Você aceita a Cebola?"
					/>
				</View>
			</View>

	);}
}


const styles = StyleSheet.create({
	container: {
	    borderRadius: 4,
	    borderWidth: 0.5,
	    backgroundColor: '#519e8a',
	    height: '100%'

	},
	form:{
		justifyContent: 'center',
    	marginBottom: 80,
    	padding: 20,
		
	},
	inner: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center' 
	},

	image: {
	    width: 200,
	    height: 200
  	}
});
